/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ceedcv.ceed150prgpro7.controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import org.ceedcv.ceed150prgpro7.modelo.IModelo;
import org.ceedcv.ceed150prgpro7.vista.VistaGraficaAcerca;

/**
 *
 * @author usu1601
 */
public class ControladorAcerca implements ActionListener{
    IModelo modelo;
    VistaGraficaAcerca vistacerca;
    
    public ControladorAcerca (VistaGraficaAcerca vistacerca , IModelo modelo) throws IOException{
        
        this.vistacerca = vistacerca;
        this.modelo = modelo;
        
        vistacerca.CrearAcerca();
        vistacerca.ventana.setVisible(true);
        vistacerca.botonSalir.addActionListener(this);
       
      
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (vistacerca.botonSalir == e.getSource()){
           vistacerca.ventana.setVisible(false); 
        }
    }
}


