package org.ceedcv.ceed150prgpro7.controlador;

import java.awt.Component;
import java.awt.Desktop;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import org.ceedcv.ceed150prgpro7.modelo.IModelo;
import org.ceedcv.ceed150prgpro7.modelo.ModeloFichero;
import org.ceedcv.ceed150prgpro7.vista.*;



/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Usuario
 */
public class Controlador implements ActionListener {

    VistaTerminal vt = new VistaTerminal();
    //VistaMenu menu;
    IModelo modelo;
    
    VistaGraficaMenu menugrafica;
    
    
    public Controlador(IModelo modelo, VistaGraficaMenu menugrafica) throws IOException {
        this.menugrafica = menugrafica;
        this.modelo = modelo;
        new ModeloFichero();
        menugrafica.CrearMenuPrincipal();
        
        this.menugrafica.botonMedico.addActionListener(this);
        this.menugrafica.botonPaciente.addActionListener(this);
        this.menugrafica.botonCita.addActionListener(this);
        this.menugrafica.botonDocuemtacion.addActionListener(this);
        this.menugrafica.botonAcerca.addActionListener(this);
        this.menugrafica.botonSalir.addActionListener(this);
        
        //opcion = menuEstructura();
        
        
        //inicio();
    }
    
    /*public char menuEstructura() throws IOException{
       
        char opcion = ' ';
        boolean test = true;
       
        do {
            
            opcion = menu.MenuEstructura();
           
            switch (opcion) {
                case 'a':
                    // Si es array
                    //modelo = new ModeloArray();
                    //test = true;
                    inicio();
                    break;
                case 'h':
                    // Si es hashset
                    //modelo = new ModeloHashSet();
                    //test = true;
                    inicio();
                    break;
                case 'f':
                    modelo = new ModeloFichero();
                    //test = true;
                    inicio();
                   break;
                case 's':
                    vt.mostrarTexto("\nHasta luego...\n");
                    test = false;
                    break;
                default: 
                    vt.mostrarTexto("\nIntroduce una opción del menú.\n");
                    break;
                }
               
        } while (test == true);
        return opcion;
    }

    public char inicio() throws IOException {

        char opcion = ' ';
        boolean test = true;
        do {
            
            opcion = menu.MenuPrincipal();

            switch (opcion) {
                case 'p':
                    paciente();
                    break;
                case 'm':
                    medico();
                    break;
                case 'c':
                    cita();
                    break;
                case 's':
                    vt.mostrarTexto("\nHasta luego...\n");
                    test = false;
                    break;
                default: 
                    vt.mostrarTexto("\nIntroduce una opción del menú.\n");
                    break;
                    
                }
        } while (test == true);
        return opcion;
    }*/

    @Override
    public void actionPerformed(ActionEvent e) {
      
        if(menugrafica.botonPaciente == e.getSource()){
            
            try {
                VistaGraficaPaciente vgp = new VistaGraficaPaciente();
                ControladorPaciente cp = new ControladorPaciente(vgp, modelo);
                
                } catch (IOException ex) {
                    Logger.getLogger(Controlador.class.getName()).log(Level.SEVERE, null, ex);
                }
        
        } else if(menugrafica.botonMedico == e.getSource()){
            try { 
                VistaGraficaMedico vgm = new VistaGraficaMedico();
                ControladorMedico cm = new ControladorMedico(vgm, modelo);
            } catch (IOException ex) {
                Logger.getLogger(Controlador.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else if(menugrafica.botonCita == e.getSource()){
            
            try {
                VistaGraficaCita vgc = new VistaGraficaCita();
                ControladorCita cv = new ControladorCita(vgc, modelo);
            } catch (IOException ex) {
                Logger.getLogger(Controlador.class.getName()).log(Level.SEVERE, null, ex);
            }
            
        } else if(menugrafica.botonAcerca == e.getSource()){
           
            try {
                 VistaGraficaAcerca vga = new VistaGraficaAcerca();
                ControladorAcerca ca = new ControladorAcerca(vga, modelo);
            } catch (IOException ex) {
                Logger.getLogger(Controlador.class.getName()).log(Level.SEVERE, null, ex);
            }
            
            
         
        } else if (menugrafica.botonDocuemtacion == e.getSource()){
            String pdf= "src/main/java/Documentacion/1516ceed150-DarioNavarro-Cita-Documentacion.pdf";
            try {
                File path = new File (pdf);
                Desktop.getDesktop().open(path);
            }catch (IOException ex) {
                ex.printStackTrace();
            }
        }else if(menugrafica.botonSalir == e.getSource()){
           Object [] opciones ={"Aceptar","Cancelar"};
            Component rootPane = null;
            int eleccion = JOptionPane.showOptionDialog(rootPane,"En realidad desea realizar cerrar la aplicacion","Mensaje de Confirmacion",
            JOptionPane.YES_NO_OPTION,
            JOptionPane.QUESTION_MESSAGE,null,opciones,"Aceptar");
                if (eleccion == JOptionPane.YES_OPTION){
            
                    System.exit(0);
                }else{ }
        }
    }
    
    /*private void paciente() throws IOException {
        VistaPaciente vistapaciente = new VistaPaciente();
        ControladorPaciente cp = new ControladorPaciente(vistapaciente, modelo);
    }

    private void medico() throws IOException {
        VistaMedico vistamedico = new VistaMedico();
        ControladorMedico cm = new ControladorMedico(vistamedico, modelo);
    }

    private void cita() throws IOException {
        VistaCita vistacita = new VistaCita();
        ControladorCita cv = new ControladorCita(vistacita, modelo);
    }
*/
    

   
}
