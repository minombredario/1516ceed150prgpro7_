
package org.ceedcv.ceed150prgpro7.vista;

/*
 * Fichero: IVista.java
 *
 * @author Dario Navarro Andres minombredario@gmail.com
 *
 */
public interface IVista<T> {

    public T obtener();

    public void mostrar(T t);
    }
